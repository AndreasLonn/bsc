use scgen::block::field::StopOption;
use scgen::block::{CustomBlock, Statement, ValueExpr};
use scgen::target::Stage;
use scgen::Project;
use std::fs::File;
use std::io;

fn main() -> io::Result<()> {
    let mut stage = Stage::empty();

    let test_block = CustomBlock::new("Test Block %s", &["Param"]);
    let test_arg = test_block.argument(0);

    stage.add_chain(&[
        Statement::WhenFlagClicked,
        Statement::Custom(test_block.clone(), vec![ValueExpr::Number(2.0)]),
    ]);
    stage.add_chain(&[
        Statement::Define(test_block.clone()),
        Statement::Wait(ValueExpr::Argument(test_arg.clone())),
        Statement::Stop(StopOption::All),
    ]);

    let project = Project::from_stage(stage);
    let file = File::create("target/test.sb3")?;
    project.write_sb3(file)
}
