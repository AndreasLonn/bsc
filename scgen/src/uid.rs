use crate::StrRef;
use rand::{seq::SliceRandom, thread_rng};
use serde::ser::{Serialize, Serializer};

/// A unique identifier.
#[derive(Copy, Clone, Eq, PartialEq, Debug, Hash)]
pub struct Uid([u8; UID_LEN]);

const UID_CHARS: &[u8] =
    b"!#%()*+,-./0123456789:;=?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[]^_`abcdefghijklmnopqrstuvwxyz{|}";
const UID_LEN: usize = 20;

impl Uid {
    /// Creates a new, random `Uid`;
    pub fn random() -> Uid {
        let mut chars = [0; UID_LEN];
        let mut rng = thread_rng();
        for c in chars.iter_mut() {
            *c = *UID_CHARS.choose(&mut rng).unwrap();
        }
        Uid(chars)
    }
}

impl AsRef<str> for Uid {
    fn as_ref(&self) -> &str {
        // Safe, as the UID is guaranteed to only contain printable ASCII
        unsafe { std::str::from_utf8_unchecked(&self.0) }
    }
}
impl From<Uid> for StrRef {
    fn from(uid: Uid) -> StrRef {
        StrRef::clone_str(uid.as_ref())
    }
}

impl Serialize for Uid {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ser.serialize_str(self.as_ref())
    }
}
