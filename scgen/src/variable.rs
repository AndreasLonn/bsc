use crate::block::{Block, Field, Input, ToField, ToInput};
use crate::{StrRef, Uid};
use serde::ser::{Serialize, Serializer};
use std::collections::HashMap;

/// A regular variable.
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct Variable {
    uid: Uid,
    name: StrRef,
}

impl Variable {
    /// Creates a new variable with the given name.
    pub fn new<S>(name: S) -> Variable
    where
        S: Into<StrRef>,
    {
        Variable {
            uid: Uid::random(),
            name: name.into(),
        }
    }
    /// Get the UID of `self`.
    pub fn uid(&self) -> Uid {
        self.uid
    }
    /// Get the name of `self`
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }
}

impl Serialize for Variable {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        (self.name(), 0).serialize(ser)
    }
}

impl ToInput for &Variable {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        Some((Input::Variable(self.clone()), HashMap::new()))
    }
}

impl ToField for &Variable {
    fn to_field(self) -> Field {
        Field::Variable(self.clone())
    }
}
