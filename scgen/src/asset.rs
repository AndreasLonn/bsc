use serde::ser::{Serialize, SerializeMap, Serializer};
use std::io::{self, Write};

/// An asset included in the project file.
#[derive(Clone, Debug)]
pub struct Asset {
    data: Box<[u8]>,
    format: AssetFormat,
    name: String,
    hash: String,
}

/// The format of the data in an `Asset`.
#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum AssetFormat {
    /// A vector image (svg).
    Vector,
}

impl Asset {
    /// Creates a new asset from the data with the given name and format
    pub fn from_data<B>(data: B, name: String, format: AssetFormat) -> Asset
    where
        B: Into<Box<[u8]>>,
    {
        let data = data.into();
        let hash = format!("{:x}", md5::compute(&data));
        Asset {
            data,
            format,
            name,
            hash,
        }
    }
}

impl Asset {
    /// The filename of `self`.
    pub fn filename(&self) -> String {
        format!("{}.{}", self.hash, self.format.as_str())
    }
    /// Writes `self` to the writer
    pub fn write_to<W>(&self, writer: &mut W) -> io::Result<()>
    where
        W: Write,
    {
        writer.write_all(&self.data)
    }
}

impl Serialize for Asset {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut ser = ser.serialize_map(None)?;

        ser.serialize_entry("assetId", &self.hash)?;
        ser.serialize_entry("name", &self.name)?;
        ser.serialize_entry("md5ext", &self.filename())?;
        ser.serialize_entry("dataFormat", &self.format.as_str())?;

        match self.format {
            AssetFormat::Vector => {
                ser.serialize_entry("rotationCenterX", &0)?;
                ser.serialize_entry("rotationCenterY", &0)?;
            }
        }

        ser.end()
    }
}

impl AssetFormat {
    /// The string representation of the format
    pub fn as_str(self) -> &'static str {
        match self {
            AssetFormat::Vector => "svg",
        }
    }
}
