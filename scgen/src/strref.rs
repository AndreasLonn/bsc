use serde::ser::{Serialize, Serializer};
use std::fmt::{self, Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::rc::Rc;

/// A static or reference counted `str`.
#[derive(Clone, Eq, PartialEq, Hash)]
pub struct StrRef(StrRefValue);

impl StrRef {
    /// Creates a reference counted copy of `str`.
    pub fn clone_str(s: &str) -> StrRef {
        StrRef(StrRefValue::RefCounted(s.into()))
    }
}

impl AsRef<str> for StrRef {
    fn as_ref(&self) -> &str {
        self.0.as_ref()
    }
}

impl Debug for StrRef {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{:?}", self.as_ref())
    }
}

impl Display for StrRef {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        write!(fmt, "{}", self.as_ref())
    }
}

impl From<&'static str> for StrRef {
    fn from(reference: &'static str) -> StrRef {
        StrRef(StrRefValue::Static(reference))
    }
}
impl From<Rc<str>> for StrRef {
    fn from(rc: Rc<str>) -> StrRef {
        StrRef(StrRefValue::RefCounted(rc))
    }
}

impl Serialize for StrRef {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ser.serialize_str(self.as_ref())
    }
}

#[derive(Clone)]
enum StrRefValue {
    Static(&'static str),
    RefCounted(Rc<str>),
}

impl AsRef<str> for StrRefValue {
    fn as_ref(&self) -> &str {
        match self {
            StrRefValue::Static(reference) => reference,
            StrRefValue::RefCounted(rc) => rc.as_ref(),
        }
    }
}

impl PartialEq for StrRefValue {
    fn eq(&self, other: &StrRefValue) -> bool {
        self.as_ref() == other.as_ref()
    }
}
impl Eq for StrRefValue {}

impl Hash for StrRefValue {
    fn hash<H>(&self, hasher: &mut H)
    where
        H: Hasher,
    {
        self.as_ref().hash(hasher);
    }
}
