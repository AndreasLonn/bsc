use crate::block::{Block, Field, Input, ToField, ToInput};
use crate::{StrRef, Uid};
use serde::ser::{Serialize, Serializer};
use std::collections::HashMap;

/// A broadcast message.
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub struct Message {
    uid: Uid,
    name: StrRef,
}

impl Message {
    /// Creates a new message with the given name.
    pub fn new<S>(name: S) -> Message
    where
        S: Into<StrRef>,
    {
        Message {
            uid: Uid::random(),
            name: name.into(),
        }
    }
    /// Get the UID of `self`.
    pub fn uid(&self) -> Uid {
        self.uid
    }
    /// Get the name of `self`
    pub fn name(&self) -> &str {
        self.name.as_ref()
    }
}

impl Serialize for Message {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        ser.serialize_str(self.name())
    }
}

impl ToInput for &Message {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        Some((Input::Message(self.clone()), HashMap::new()))
    }
}

impl ToField for &Message {
    fn to_field(self) -> Field {
        Field::Message(self.clone())
    }
}
