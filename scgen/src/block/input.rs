use crate::block::{Block, ChainBuilder, Statement};
use crate::{Message, Uid, Variable};
use serde::ser::{Serialize, Serializer};
use std::collections::HashMap;

/// A raw code block input.
#[derive(Clone, Debug)]
pub enum Input {
    /// An expression code block or a code block chain.
    Block(Uid),
    /// An integer literal.
    Int(isize),
    /// A broadcast message.
    Message(Message),
    /// A number literal.
    Number(f64),
    /// The value of a variable.
    Variable(Variable),
}

impl Serialize for Input {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Input::Block(uid) => (1, uid).serialize(ser),
            Input::Int(n) => (1, (7, n.to_string())).serialize(ser),
            Input::Message(message) => (1, (11, message.name(), message.uid())).serialize(ser),
            Input::Number(n) => (1, (4, n.to_string())).serialize(ser),
            Input::Variable(variable) => (1, (12, variable.name(), variable.uid())).serialize(ser),
        }
    }
}

/// Trait used by `BlockBuilder` to create block inputs.
pub trait ToInput {
    /// Converts `self` into an `Input`, returning the `Input` and all generated blocks. Returns
    /// `None` if the input is detached.
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)>;
}

impl ToInput for isize {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        Some((Input::Int(self), HashMap::new()))
    }
}

impl ToInput for f64 {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        Some((Input::Number(self), HashMap::new()))
    }
}

impl ToInput for &Vec<Statement> {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        let mut builder = ChainBuilder::empty();
        builder.append_all(self);
        builder.to_input()
    }
}
