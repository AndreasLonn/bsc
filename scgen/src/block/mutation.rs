use crate::StrRef;
use serde::ser::{Serialize, SerializeMap, Serializer};
use std::collections::HashMap;

///Struct containing block mutations
#[derive(Clone, Debug, Default)]
pub struct Mutations {
    mutations: HashMap<StrRef, String>,
}

impl Mutations {
    /// Creates a new `Mutation` with no mutation set.
    pub fn new() -> Mutations {
        Mutations {
            mutations: HashMap::new(),
        }
    }
    /// Adds a mutation to `self`.
    pub fn mutate<S>(&mut self, option: S, value: String)
    where
        S: Into<StrRef>,
    {
        self.mutations.insert(option.into(), value);
    }
    /// Tests if `self` is empty.
    pub fn is_empty(&self) -> bool {
        self.mutations.is_empty()
    }
}

impl Serialize for Mutations {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut ser = ser.serialize_map(None)?;

        ser.serialize_entry("tagName", "mutation")?;
        ser.serialize_entry("children", &[] as &[()])?;

        for (name, ref value) in &self.mutations {
            ser.serialize_entry(name, value)?;
        }

        ser.end()
    }
}
