//! Module containing the `Field` struct and `ToField` trait, as well as field implementations.

use crate::{Message, StrRef, Variable};
use serde::ser::{Serialize, Serializer};

/// A raw code block field.
#[derive(Clone, Eq, PartialEq, Debug, Hash)]
pub enum Field {
    /// A broadcast message
    Message(Message),
    /// A regular string field value.
    String(StrRef),
    /// A variable.
    Variable(Variable),
}

impl Serialize for Field {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        match self {
            Field::Message(message) => (message.name(), message.uid()).serialize(ser),
            Field::String(string) => (string, ()).serialize(ser),
            Field::Variable(variable) => (variable.name(), variable.uid()).serialize(ser),
        }
    }
}

/// Trait used by `BlockBuilder` to create block fields.
pub trait ToField {
    /// Converts `self` into a `Field`.
    fn to_field(self) -> Field;
}

impl ToField for Field {
    fn to_field(self) -> Field {
        self
    }
}
impl ToField for StrRef {
    fn to_field(self) -> Field {
        Field::String(self)
    }
}

macro_rules! define_field {
    {
        $(#[$outer:meta])*
        pub enum $name: ident {
            $(
                $(#[$inner:meta])*
                $option: ident = $value: expr,
            )+
        }
    } => {
        $(#[$outer])*
        #[derive(Copy, Clone, Eq, PartialEq, Debug)]
        pub enum $name {
            $($(#[$inner])* $option,)+
        }
        impl ToField for $name {
            fn to_field(self) -> Field {
                Field::String(match self {
                    $($name::$option => $value,)+
                }.into())
            }
        }
    }
}

define_field! {
    /// A mathematical operation used by `Expression::MathOp`.
    pub enum MathOp {
        /// The absolute value
        Abs = "abs",
        /// Inverted cosine
        Acos = "acos",
        /// Inverted sine
        Asin = "asin",
        /// Inverted tangent
        Atan = "atan",
        /// Rounding up
        Ceiling = "ceiling",
        /// Cosine
        Cos = "cos",
        /// E to the power of
        Exp = "e ^",
        /// 10 to the power of
        Exp10 = "10 ^",
        /// Rounding down
        Floor = "floor",
        /// Logarithm base e
        Ln = "ln",
        /// Logarithm base 10
        Log = "log",
        /// Sine
        Sin = "sin",
        /// Square root
        Sqrt = "sqrt",
        /// Tangent
        Tan = "tan",
    }
}
define_field! {
    /// A target of `Statement::Stop`.
    pub enum StopOption {
        /// All running scripts
        All = "all",
        /// This script
        This = "this script",
        /// Other scripts in sprite
        Other = "other scripts in sprite",
    }
}
