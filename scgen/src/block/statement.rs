use crate::block::field::StopOption;
use crate::block::{BlockBuilder, BoolExpr, CustomBlock, ValueExpr};
use crate::{Message, Variable};

/// A statement block.
#[derive(Clone, Debug)]
pub enum Statement {
    /// Broadcasts a message.
    Broadcast(ValueExpr),
    /// Calls a custom block
    Custom(CustomBlock, Vec<ValueExpr>),
    /// Defines a custom block
    Define(CustomBlock),
    /// Repeats a statement chain forever.
    Forever(Vec<Statement>),
    /// Set the value of a variable.
    SetValue(Variable, ValueExpr),
    /// Stops the selected scripts.
    Stop(StopOption),
    /// Waits the specified amount of time.
    Wait(ValueExpr),
    /// Waits the specified amount of time.
    WaitUntil(BoolExpr),
    /// Triggers when a message is broadcasted.
    WhenBroadcastReceived(Message),
    /// Triggers when the flag is clicked.
    WhenFlagClicked,
}

impl Statement {
    /// Turns `self` into a [`BlockBuilder`](crate::block::BlockBuilder).
    pub fn to_builder(&self) -> BlockBuilder {
        match self {
            Statement::Broadcast(message) => {
                BlockBuilder::new("event_broadcast").add_input("BROADCAST_INPUT", message)
            }
            Statement::Custom(block, arguments) => block.to_invocation(&arguments),
            Statement::Define(block) => block.to_definition(),
            Statement::Forever(statements) => {
                BlockBuilder::new("control_forever").add_input("SUBSTACK", statements)
            }
            Statement::SetValue(variable, value) => BlockBuilder::new("data_setvariableto")
                .add_field("VARIABLE", variable)
                .add_input("VALUE", value),
            Statement::Stop(target) => BlockBuilder::new("control_stop")
                .add_field("STOP_OPTION", *target)
                .mutate_json("hasnext", *target == StopOption::Other),
            Statement::Wait(duration) => {
                BlockBuilder::new("control_wait").add_input("DURATION", duration)
            }
            Statement::WaitUntil(condition) => {
                BlockBuilder::new("control_wait_until").add_input("CONDITION", condition)
            }
            Statement::WhenBroadcastReceived(message) => {
                BlockBuilder::new("event_whenbroadcastreceived")
                    .add_field("BROADCAST_OPTION", message)
            }
            Statement::WhenFlagClicked => BlockBuilder::new("event_whenflagclicked"),
        }
    }
}
