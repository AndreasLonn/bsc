//! Module containing all code blocks.

use crate::{StrRef, Uid};
use serde::ser::{Serialize, SerializeMap, Serializer};
use std::collections::HashMap;

mod builder;
mod custom;
mod expression;
pub mod field;
mod input;
mod mutation;
mod statement;
pub use builder::{BlockBuilder, ChainBuilder};
pub use custom::{Argument, CustomBlock};
pub use expression::{BoolExpr, ValueExpr};
pub use field::{Field, ToField};
pub use input::{Input, ToInput};
pub use mutation::Mutations;
pub use statement::Statement;

/// A raw code block.
#[derive(Clone, Debug)]
pub struct Block {
    opcode: &'static str,
    parent: Option<Uid>,
    next: Option<Uid>,
    inputs: HashMap<StrRef, Input>,
    fields: HashMap<StrRef, Field>,
    mutations: Mutations,
}

impl Block {
    /// Creates a new `Block` with the given opcode.
    pub fn new(opcode: &'static str) -> Block {
        Block {
            opcode,
            parent: None,
            next: None,
            inputs: HashMap::new(),
            fields: HashMap::new(),
            mutations: Mutations::new(),
        }
    }
    /// Adds an input to `self`, replacing the input with the same name if it already exists.
    pub fn add_input<S>(&mut self, name: S, input: Input)
    where
        S: Into<StrRef>,
    {
        self.inputs.insert(name.into(), input);
    }
    /// Adds a field to `self`, replacing the field with the same name if it already exists.
    pub fn add_field<S>(&mut self, name: S, field: Field)
    where
        S: Into<StrRef>,
    {
        self.fields.insert(name.into(), field);
    }
    /// Adds a mutation to `self` with the given value.
    pub fn add_mutation<S>(&mut self, name: S, value: String)
    where
        S: Into<StrRef>,
    {
        self.mutations.mutate(name, value);
    }

    /// Set the parent of `self`.
    pub fn set_parent(&mut self, parent: Uid) {
        self.parent = Some(parent);
    }
    /// Set the next block after `self` in a code block chain.
    pub fn set_next(&mut self, next: Uid) {
        self.next = Some(next);
    }
}

impl Serialize for Block {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut ser = ser.serialize_map(None)?;

        let toplevel = self.parent.is_none();
        ser.serialize_entry("opcode", self.opcode)?;
        ser.serialize_entry("parent", &self.parent)?;
        ser.serialize_entry("topLevel", &toplevel)?;
        ser.serialize_entry("next", &self.next)?;
        ser.serialize_entry("inputs", &self.inputs)?;
        ser.serialize_entry("fields", &self.fields)?;
        ser.serialize_entry("shadow", &false)?;

        if !self.mutations.is_empty() {
            ser.serialize_entry("mutation", &self.mutations)?;
        }

        if toplevel {
            ser.serialize_entry("x", &0)?;
            ser.serialize_entry("y", &0)?;
        }

        ser.end()
    }
}
