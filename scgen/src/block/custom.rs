use crate::block::{Block, BlockBuilder, Field, Input, ToInput, ValueExpr};
use crate::{StrRef, Uid};
use std::collections::HashMap;
use std::rc::Rc;

/// A custom code block.
#[derive(Clone, Debug)]
pub struct CustomBlock {
    proccode: StrRef,
    arg_names: Rc<[StrRef]>,
    arg_ids: Rc<[Uid]>,
}

impl CustomBlock {
    /// Creates a new custom block with the given proccode and arguments. Arguments should be
    /// formatted as `%s` in the proccode.
    pub fn new<S>(proccode: S, args: &[&str]) -> CustomBlock
    where
        S: Into<StrRef>,
    {
        let arg_names = args
            .iter()
            .map(|&n| StrRef::clone_str(n))
            .collect::<Vec<_>>();
        let arg_ids = arg_names.iter().map(|_| Uid::random()).collect::<Vec<_>>();

        CustomBlock {
            proccode: proccode.into(),
            arg_names: arg_names.into(),
            arg_ids: arg_ids.into(),
        }
    }
    /// The names of the arguments of self.
    pub fn arg_names(&self) -> &[StrRef] {
        self.arg_names.as_ref()
    }
    /// The ids of the arguments of self.
    pub fn arg_ids(&self) -> &[Uid] {
        self.arg_ids.as_ref()
    }

    /// Creates a definition block from self.
    pub fn to_definition(&self) -> BlockBuilder {
        let mut prototype = BlockBuilder::new("procedures_prototype")
            .mutate("proccode", self.proccode.to_string())
            .mutate_json("argumentids", self.arg_ids())
            .mutate_json("argumentnames", self.arg_names())
            .mutate_json("argumentdefaults", vec![""; self.arg_ids().len()])
            .mutate_json("warp", false);

        for (name, uid) in self.arg_names().iter().zip(self.arg_ids()) {
            prototype = prototype.add_input(
                StrRef::clone_str(uid.as_ref()),
                BlockBuilder::new("argument_reporter_string_number")
                    .add_field("VALUE", name.clone()),
            );
        }

        BlockBuilder::new("procedures_definition").add_input("custom_block", prototype)
    }

    /// Creates an invocation block from self.
    pub fn to_invocation(&self, params: &[ValueExpr]) -> BlockBuilder {
        let mut builder = BlockBuilder::new("procedures_call")
            .mutate("proccode", self.proccode.to_string())
            .mutate_json("argumentids", self.arg_ids())
            .mutate_json("warp", false);

        assert_eq!(params.len(), self.arg_ids().len());

        for (i, uid) in self.arg_ids().iter().enumerate() {
            builder = builder.add_input(StrRef::clone_str(uid.as_ref()), &params[i]);
        }

        builder
    }

    /// Get a specific argument used by self.
    pub fn argument(&self, index: usize) -> Argument {
        Argument(self.arg_names()[index].clone())
    }
}

/// An argument of a `CustomBlock`.
#[derive(Clone, Debug)]
pub struct Argument(StrRef);

impl ToInput for &Argument {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        let (uid, blocks) = BlockBuilder::new("argument_reporter_string_number")
            .add_field("VALUE", Field::String(self.0.clone()))
            .build();
        Some((Input::Block(uid), blocks))
    }
}
