use crate::block::{Block, Input, Statement, ToField, ToInput};
use crate::{StrRef, Uid};
use serde::ser::Serialize;
use std::collections::HashMap;

/// Helper struct used to build raw code blocks.
pub struct BlockBuilder {
    uid: Uid,
    block: Block,
    children: HashMap<Uid, Block>,
}

impl BlockBuilder {
    /// Creates a new `BlockBuilder`.
    pub fn new(opcode: &'static str) -> BlockBuilder {
        BlockBuilder {
            uid: Uid::random(),
            block: Block::new(opcode),
            children: HashMap::new(),
        }
    }
    /// Adds an input to `self` with the given name and value.
    pub fn add_input<S, I>(mut self, name: S, value: I) -> BlockBuilder
    where
        S: Into<StrRef>,
        I: ToInput,
    {
        if let Some((input, mut blocks)) = value.to_input() {
            if let Input::Block(uid) = input {
                blocks.get_mut(&uid).unwrap().set_parent(self.uid);
            }
            self.block.add_input(name, input);
            self.children.extend(blocks);
        }
        self
    }
    /// Adds a field to `self` with the given name and value.
    pub fn add_field<S, F>(mut self, name: S, value: F) -> BlockBuilder
    where
        S: Into<StrRef>,
        F: ToField,
    {
        self.block.add_field(name, value.to_field());
        self
    }
    /// Adds a mutation to `self` with the given JSON-encoded value.
    pub fn mutate_json<S, V>(mut self, name: S, value: V) -> BlockBuilder
    where
        S: Into<StrRef>,
        V: Serialize,
    {
        let value = serde_json::to_string(&value).unwrap();
        self.block.add_mutation(name, value);
        self
    }
    /// Adds a mutation to `self` with the given value.
    pub fn mutate<S>(mut self, name: S, value: String) -> BlockBuilder
    where
        S: Into<StrRef>,
    {
        self.block.add_mutation(name, value);
        self
    }
    /// Returns all generated blocks and the toplevel `Uid`.
    pub fn build(self) -> (Uid, HashMap<Uid, Block>) {
        let mut blocks = self.children;
        blocks.insert(self.uid, self.block);
        (self.uid, blocks)
    }
}

impl ToInput for BlockBuilder {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        let (uid, blocks) = self.build();
        Some((Input::Block(uid), blocks))
    }
}

/// Helper struct to build statement chains.
pub struct ChainBuilder {
    first: Option<Uid>,
    last: Option<Uid>,
    blocks: HashMap<Uid, Block>,
}

impl ChainBuilder {
    /// Creates an empty `ChainBuilder`.
    pub fn empty() -> ChainBuilder {
        ChainBuilder {
            first: None,
            last: None,
            blocks: HashMap::new(),
        }
    }
    /// Appends the statement to `self`.
    pub fn append(&mut self, statement: &Statement) {
        let (uid, blocks) = statement.to_builder().build();
        self.blocks.extend(blocks);
        if let Some(last) = self.last {
            self.blocks.get_mut(&last).unwrap().set_next(uid);
            self.blocks.get_mut(&uid).unwrap().set_parent(last);
        } else {
            self.first = Some(uid);
        }
        self.last = Some(uid);
    }
    /// Appends all statements in the chain to `self`.
    pub fn append_all<'a, I>(&mut self, chain: I)
    where
        I: IntoIterator<Item = &'a Statement>,
    {
        for statement in chain {
            self.append(statement);
        }
    }

    /// Returns all generated blocks and the toplevel `Uid`. Returns `None` if the chain is empty.
    pub fn build(self) -> Option<(Uid, HashMap<Uid, Block>)> {
        self.first.map(|uid| (uid, self.blocks))
    }
}

impl ToInput for ChainBuilder {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        self.build()
            .map(|(uid, blocks)| (Input::Block(uid), blocks))
    }
}
