use crate::block::field::MathOp;
use crate::block::{Argument, Block, BlockBuilder, Input, ToInput};
use crate::{Message, Uid, Variable};
use std::collections::HashMap;

/// A value expression block.
#[derive(Clone, Debug)]
pub enum ValueExpr {
    /// Addition of two numbers.
    Add(Box<ValueExpr>, Box<ValueExpr>),
    /// A custom block argument
    Argument(Argument),
    /// Division of two numbers.
    Div(Box<ValueExpr>, Box<ValueExpr>),
    /// An integer literal.
    Int(isize),
    /// A math operation
    MathOp(MathOp, Box<ValueExpr>),
    /// A broadcast message
    Message(Message),
    /// Multiplication of two numbers.
    Mul(Box<ValueExpr>, Box<ValueExpr>),
    /// A number literal.
    Number(f64),
    /// Subtraction of two numbers.
    Sub(Box<ValueExpr>, Box<ValueExpr>),
    /// The value of a variable.
    Variable(Variable),
}

impl ToInput for &ValueExpr {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        match self {
            ValueExpr::Add(num1, num2) => BlockBuilder::new("operator_add")
                .add_input("NUM1", num1.as_ref())
                .add_input("NUM2", num2.as_ref())
                .to_input(),
            ValueExpr::Argument(argument) => argument.to_input(),
            ValueExpr::Div(num1, num2) => BlockBuilder::new("operator_divide")
                .add_input("NUM1", num1.as_ref())
                .add_input("NUM2", num2.as_ref())
                .to_input(),
            ValueExpr::Int(value) => value.to_input(),
            ValueExpr::MathOp(operator, num) => BlockBuilder::new("operator_mathop")
                .add_field("OPERATOR", *operator)
                .add_input("NUM", num.as_ref())
                .to_input(),
            ValueExpr::Message(message) => message.to_input(),
            ValueExpr::Mul(num1, num2) => BlockBuilder::new("operator_multiply")
                .add_input("NUM1", num1.as_ref())
                .add_input("NUM2", num2.as_ref())
                .to_input(),
            ValueExpr::Number(value) => value.to_input(),
            ValueExpr::Sub(num1, num2) => BlockBuilder::new("operator_subtract")
                .add_input("NUM1", num1.as_ref())
                .add_input("NUM2", num2.as_ref())
                .to_input(),
            ValueExpr::Variable(variable) => variable.clone().to_input(),
        }
    }
}

/// A boolean expression block.
#[derive(Clone, Debug)]
pub enum BoolExpr {
    /// True if both operands are true.
    And(Box<BoolExpr>, Box<BoolExpr>),
    /// Tests if two values are equal.
    Equal(Box<ValueExpr>, Box<ValueExpr>),
    /// Tests if the first operand is greater than the second.
    Greater(Box<ValueExpr>, Box<ValueExpr>),
    /// Tests if the first operand is less than the second.
    Less(Box<ValueExpr>, Box<ValueExpr>),
    /// Negates the operand.
    Not(Box<BoolExpr>),
    /// True if at least one operand is true.
    Or(Box<BoolExpr>, Box<BoolExpr>),
}

impl ToInput for &BoolExpr {
    fn to_input(self) -> Option<(Input, HashMap<Uid, Block>)> {
        match self {
            BoolExpr::And(op1, op2) => BlockBuilder::new("operator_and")
                .add_input("OPERAND1", op1.as_ref())
                .add_input("OPERAND2", op2.as_ref())
                .to_input(),
            BoolExpr::Equal(num1, num2) => BlockBuilder::new("operator_equals")
                .add_input("OPERAND1", num1.as_ref())
                .add_input("OPERAND2", num2.as_ref())
                .to_input(),
            BoolExpr::Greater(num1, num2) => BlockBuilder::new("operator_gt")
                .add_input("OPERAND1", num1.as_ref())
                .add_input("OPERAND2", num2.as_ref())
                .to_input(),
            BoolExpr::Less(num1, num2) => BlockBuilder::new("operator_lt")
                .add_input("OPERAND1", num1.as_ref())
                .add_input("OPERAND2", num2.as_ref())
                .to_input(),
            BoolExpr::Not(op) => BlockBuilder::new("operator_or")
                .add_input("OPERAND", op.as_ref())
                .to_input(),
            BoolExpr::Or(op1, op2) => BlockBuilder::new("operator_or")
                .add_input("OPERAND1", op1.as_ref())
                .add_input("OPERAND2", op2.as_ref())
                .to_input(),
        }
    }
}
