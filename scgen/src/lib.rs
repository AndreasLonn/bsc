//! Crate used by the BS compiler to create [Scratch] project files.
//!
//! [Scratch]: https://scratch.mit.edu

#![warn(missing_docs)]

pub mod block;
pub mod target;

mod asset;
mod message;
mod project;
mod strref;
mod uid;
mod variable;
pub use asset::{Asset, AssetFormat};
pub use message::Message;
pub use project::Project;
pub use strref::StrRef;
pub use uid::Uid;
pub use variable::Variable;
