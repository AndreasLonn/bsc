use crate::target::{Stage, Target};
use serde::ser::{Serialize, SerializeMap, Serializer};
use std::collections::HashSet;
use std::io::{self, Seek, Write};
use zip::write::{FileOptions, ZipWriter};

/// A [Scratch] project.
///
/// [Scratch]: https://scratch.mit.edu
pub struct Project {
    stage: Stage,
}

/// Struct used to serialize [Project] version and user agent metadata.
struct ProjectMeta;

impl Project {
    /// Creates a new `Project` with the given `Stage`.
    pub fn from_stage(stage: Stage) -> Project {
        Project { stage }
    }

    /// Writes `self` to the writer in `.sb3` format.
    pub fn write_sb3<W>(&self, writer: W) -> io::Result<()>
    where
        W: Write + Seek,
    {
        let mut zip = ZipWriter::new(writer);

        zip.start_file("project.json", FileOptions::default())?;

        serde_json::to_writer(&mut zip, &self)?;

        let mut seen_assets = HashSet::new();
        for asset in self.stage.assets() {
            let filename = asset.filename();

            zip.start_file(filename, FileOptions::default())?;
            asset.write_to(&mut zip)?;

            seen_assets.insert(asset.filename());
        }

        Ok(())
    }
}

impl Serialize for Project {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut ser = ser.serialize_map(None)?;

        ser.serialize_entry("targets", &[&self.stage as &Target])?;
        ser.serialize_entry("monitors", &[] as &[()])?;
        ser.serialize_entry("extensions", &[] as &[()])?;
        ser.serialize_entry("meta", &ProjectMeta)?;

        ser.end()
    }
}

impl Serialize for ProjectMeta {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut ser = ser.serialize_map(None)?;

        ser.serialize_entry("semver", "3.0.0")?;
        ser.serialize_entry("vm", "0.2.0")?;
        ser.serialize_entry("agent", "BS Compiler")?;

        ser.end()
    }
}
