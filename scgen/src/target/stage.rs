use crate::block::{Block, ChainBuilder, Statement};
use crate::target::Target;
use crate::{Asset, AssetFormat, Message, Uid, Variable};
use std::collections::HashMap;

/// A stage target.
#[derive(Clone, Debug)]
pub struct Stage {
    background: Asset,
    blocks: HashMap<Uid, Block>,
    messages: HashMap<Uid, Message>,
    variables: HashMap<Uid, Variable>,
}

const BG_SOURCE: &[u8] = br#"<svg width="1" height="1" viewBox="0 0 1 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>"#;

impl Stage {
    /// Creates a new empty `Stage`.
    pub fn empty() -> Stage {
        let background = Asset::from_data(BG_SOURCE, "background".to_string(), AssetFormat::Vector);

        Stage {
            background,
            blocks: HashMap::new(),
            messages: HashMap::new(),
            variables: HashMap::new(),
        }
    }

    /// Add a statement chain to `self`.
    pub fn add_chain<'a, I>(&mut self, chain: I)
    where
        I: IntoIterator<Item = &'a Statement>,
    {
        let mut builder = ChainBuilder::empty();
        builder.append_all(chain);
        if let Some((_, blocks)) = builder.build() {
            self.blocks.extend(blocks);
        }
    }

    /// Add a message to `self`.
    pub fn add_message(&mut self, message: Message) {
        self.messages.insert(message.uid(), message);
    }

    /// Add a variable to `self`.
    pub fn add_variable(&mut self, variable: Variable) {
        self.variables.insert(variable.uid(), variable);
    }
}

impl Target for Stage {
    fn blocks(&self) -> &HashMap<Uid, Block> {
        &self.blocks
    }
    fn costumes(&self) -> Vec<&Asset> {
        vec![&self.background]
    }
    fn messages(&self) -> &HashMap<Uid, Message> {
        &self.messages
    }
    fn variables(&self) -> &HashMap<Uid, Variable> {
        &self.variables
    }
}
