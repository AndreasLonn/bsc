//! Module containing project targets. See the [`Target`] trait for more information.
//!
//! [`Target`]: crate::target::Target

use crate::block::Block;
use crate::{Asset, Message, Uid, Variable};
use serde::ser::{Serialize, SerializeMap, Serializer};
use std::collections::HashMap;

mod stage;
pub use stage::Stage;

/// Trait representing project targets (stages and sprites).
pub trait Target {
    /// All code blocks in `self`.
    fn blocks(&self) -> &HashMap<Uid, Block>;
    /// All costumes used by `self`.
    fn costumes(&self) -> Vec<&Asset>;
    /// All messages used by `self`.
    fn messages(&self) -> &HashMap<Uid, Message>;
    /// All variables used by `self`.
    fn variables(&self) -> &HashMap<Uid, Variable>;

    /// All assets used by `self`.
    fn assets(&self) -> Vec<&Asset> {
        self.costumes()
    }
}

impl Serialize for Target {
    fn serialize<S>(&self, ser: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut ser = ser.serialize_map(None)?;

        // TODO: Replace the dummy values with real data
        ser.serialize_entry("isStage", &true)?;
        ser.serialize_entry("name", "Stage")?;
        ser.serialize_entry("variables", self.variables())?;
        ser.serialize_entry("lists", &HashMap::new() as &HashMap<String, ()>)?;
        ser.serialize_entry("broadcasts", self.messages())?;
        ser.serialize_entry("blocks", self.blocks())?;
        ser.serialize_entry("comments", &HashMap::new() as &HashMap<String, ()>)?;
        ser.serialize_entry("currentCostume", &0)?;
        ser.serialize_entry("costumes", &self.costumes())?;
        ser.serialize_entry("sounds", &[] as &[()])?;
        ser.serialize_entry("volume", &0)?;
        ser.serialize_entry("layerOrder", &0)?;
        ser.serialize_entry("tempo", &60)?;
        ser.serialize_entry("videoTransparency", &50)?;
        ser.serialize_entry("videoState", "on")?;
        ser.serialize_entry("textToSpeechLanguage", &())?;

        ser.end()
    }
}
